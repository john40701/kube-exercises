Objetivo:
1. [Ingress Controller / Secrets] Crea los siguientes objetos de forma declarativa con las siguientes especificaciones:
  • Imagen: nginx
  • Version: 1.19.4
  • 3 replicas
  • Label: app: nginx-server
  • Exponer el puerto 80 de los pods
  • Limits:
    CPU: 20 milicores
    Memoria: 128Mi
  • Requests:
    CPU: 20 milicores
    Memoria: 128Mi

a. A continuación, tras haber expuesto el servicio en el puerto 80, se deberá
acceder a la página principal de Nginx a través de la siguiente URL:
http://<student_name>.student.lasalle.com.

R./ Dentro de la carpeta \hw-03\exercise_1 se encuentran los siguientes ficheros:

  nginx-replicaSet.yaml
  service3.ymal
  Ingress.yml

  Se ejecutan los ficheros con el sigiente comando:
    kubectl apply -f nginx-replicaSet.yaml
    kubectl apply -f service3.yamal
    kubectl apply -f Ingress.yaml

    Se realiza un tunnel para verificar que la configuración haya quedado correcta (se debe dejar abierto el tunel durante toda la prueba):
    minikube tunnel 

  Adicional se crea en el host la maquina local la siguiente entrada: 
    127.0.0.1 john.student.lasalle.com

  Se ingresa por explorador a la dirección http://john.student.lasalle.com

b. Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS
R./ Dentro de la carpeta \hw-03\exercise_1 se encuentran los siguientes ficheros:

  sallekey.key
  salle.ctr
  Ingress-secure.yaml
  secret.yaml

, para ello:
  • Crear un certificado mediante la herramienta OpenSSL u otra similar
  Se crea el certificado:
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out salle.ctr -keyout sallekey.key -subj "/CN=student.lasalle.com/O=student.lasalle.com"
  
  • Crear un secret que contenga el certificado
  Con el ctr y el key se crea el secret
   kubectl apply -f secret.yaml
  minikube addons configure ingress
  -- Enter custom cert(format is "namespace/secret"): default/student.lasalle.com

  Se agrega el certificado al ingress
   kubectl apply -f Ingress-secure.yaml

   Se ingresa por explorador a https://john.student.lasalle.com y se verifica que el Emitido por y el emitido para correponda a: student.lasalle.com