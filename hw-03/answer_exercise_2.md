Objetivo:
1. [StatefulSet] Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en
clase)
Se pide:
  • Habilitar el clúster de MongoDB

R. Dentro de la carpeta \hw-03\exercise_2 se encuentran los siguientes ficheros:
  mongo-service.yaml
  mongo-statefull.yaml

  Se ejecutan los siguientes comandos:
    kubectl apply -f mongo-service.yaml
    kubectl apply -f  mongo-statefull.yaml

    kubectl get pods
    NAME          READY   STATUS    RESTARTS      AGE
    mongod-0      1/1     Running   0             36s
    mongod-1      1/1     Running   0             23s
    mongod-2      1/1     Running   0             10s

    Se ingresa a uno de los nodos y se habilita el cluster

    kubectl exec -it mongod-0 -c mongod-container bash
    root@mongod-0:/# mongo
        rs.initiate({ _id: "MainRepSet", version: 1, 
          members: [ 
           { _id: 0, host: "mongod-0.mongodb-service.default.svc.cluster.local:27017" }, 
           { _id: 1, host: "mongod-1.mongodb-service.default.svc.cluster.local:27017" }, 
           { _id: 2, host: "mongod-2.mongodb-service.default.svc.cluster.local:27017" } ]});
 
  Se verifica la configuración del cluster
        MainRepSet:SECONDARY> rs.status();
      {
              "set" : "MainRepSet",
              "date" : ISODate("2021-11-28T20:20:01.115Z"),
              "myState" : 1,
              "term" : NumberLong(1),
              "syncSourceHost" : "",
              "syncSourceId" : -1,
              "heartbeatIntervalMillis" : NumberLong(2000),
              "majorityVoteCount" : 2,
              "writeMajorityCount" : 2,
              "votingMembersCount" : 3,
              "writableVotingMembersCount" : 3,
              "optimes" : {
                      "lastCommittedOpTime" : {
                              "ts" : Timestamp(1638130800, 7),
                              "t" : NumberLong(1)
                      },
                      "lastCommittedWallTime" : ISODate("2021-11-28T20:20:00.582Z"),
                      "readConcernMajorityOpTime" : {
                              "ts" : Timestamp(1638130800, 7),
                              "t" : NumberLong(1)
                      },
                      "appliedOpTime" : {
                              "ts" : Timestamp(1638130800, 7),
                              "t" : NumberLong(1)
                      },
                      "durableOpTime" : {
                              "ts" : Timestamp(1638130800, 7),
                              "t" : NumberLong(1)
                      },
                      "lastAppliedWallTime" : ISODate("2021-11-28T20:20:00.582Z"),
                      "lastDurableWallTime" : ISODate("2021-11-28T20:20:00.582Z")
              },
              "lastStableRecoveryTimestamp" : Timestamp(1638130788, 1),
              "electionCandidateMetrics" : {
                      "lastElectionReason" : "electionTimeout",
                      "lastElectionDate" : ISODate("2021-11-28T20:19:59.573Z"),
                      "electionTerm" : NumberLong(1),
                      "lastCommittedOpTimeAtElection" : {
                              "ts" : Timestamp(1638130788, 1),
                              "t" : NumberLong(-1)
                      },
                      "lastSeenOpTimeAtElection" : {
                              "ts" : Timestamp(1638130788, 1),
                              "t" : NumberLong(-1)
                      },
                      "numVotesNeeded" : 2,
                      "priorityAtElection" : 1,
                      "electionTimeoutMillis" : NumberLong(10000),
                      "numCatchUpOps" : NumberLong(0),
                      "newTermStartDate" : ISODate("2021-11-28T20:19:59.676Z"),
                      "wMajorityWriteAvailabilityDate" : ISODate("2021-11-28T20:20:00.301Z")
              },
              "members" : [
                      {
                              "_id" : 0,
                              "name" : "mongod-0.mongodb-service.default.svc.cluster.local:27017",
                              "health" : 1,
                              "state" : 1,
                              "stateStr" : "PRIMARY",
                              "uptime" : 454,
                              "optime" : {
                                      "ts" : Timestamp(1638130800, 7),
                                      "t" : NumberLong(1)
                              },
                              "optimeDate" : ISODate("2021-11-28T20:20:00Z"),
                              "syncSourceHost" : "",
                              "syncSourceId" : -1,
                              "infoMessage" : "",
                              "electionTime" : Timestamp(1638130799, 1),
                              "electionDate" : ISODate("2021-11-28T20:19:59Z"),
                              "configVersion" : 1,
                              "configTerm" : 1,
                              "self" : true,
                              "lastHeartbeatMessage" : ""
                      },
                      {
                              "_id" : 1,
                              "name" : "mongod-1.mongodb-service.default.svc.cluster.local:27017",
                              "health" : 1,
                              "state" : 2,
                              "stateStr" : "SECONDARY",
                              "uptime" : 12,
                              "optime" : {
                                      "ts" : Timestamp(1638130788, 1),
                                      "t" : NumberLong(-1)
                              },
                              "optimeDurable" : {
                                      "ts" : Timestamp(1638130788, 1),
                                      "t" : NumberLong(-1)
                              },
                              "optimeDate" : ISODate("2021-11-28T20:19:48Z"),
                              "optimeDurableDate" : ISODate("2021-11-28T20:19:48Z"),
                              "lastHeartbeat" : ISODate("2021-11-28T20:19:59.599Z"),
                              "lastHeartbeatRecv" : ISODate("2021-11-28T20:20:00.611Z"),
                              "pingMs" : NumberLong(0),
                              "lastHeartbeatMessage" : "",
                              "syncSourceHost" : "",
                              "syncSourceId" : -1,
                              "infoMessage" : "",
                              "configVersion" : 1,
                              "configTerm" : 0
                      },
                      {
                              "_id" : 2,
                              "name" : "mongod-2.mongodb-service.default.svc.cluster.local:27017",
                              "health" : 1,
                              "state" : 2,
                              "stateStr" : "SECONDARY",
                              "uptime" : 12,
                              "optime" : {
                                      "ts" : Timestamp(1638130788, 1),
                                      "t" : NumberLong(-1)
                              },
                              "optimeDurable" : {
                                      "ts" : Timestamp(1638130788, 1),
                                      "t" : NumberLong(-1)
                              },
                              "optimeDate" : ISODate("2021-11-28T20:19:48Z"),
                              "optimeDurableDate" : ISODate("2021-11-28T20:19:48Z"),
                              "lastHeartbeat" : ISODate("2021-11-28T20:19:59.598Z"),
                              "lastHeartbeatRecv" : ISODate("2021-11-28T20:20:00.613Z"),
                              "pingMs" : NumberLong(0),
                              "lastHeartbeatMessage" : "",
                              "syncSourceHost" : "",
                              "syncSourceId" : -1,
                              "infoMessage" : "",
                              "configVersion" : 1,
                              "configTerm" : 0
                      }
              ],
              "ok" : 1,
              "$clusterTime" : {
                      "clusterTime" : Timestamp(1638130800, 7),
                      "signature" : {
                              "hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
                              "keyId" : NumberLong(0)
                      }
              },
              "operationTime" : Timestamp(1638130800, 7)
      }

  • Realizar una operación en una de las instancias a nivel de configuración y verificar que el cambio se ha aplicado al resto de instancias
  Se el cluster principal se crea una coleccion.

  MainRepSet:PRIMARY> db.createCollection("test");
{
        "ok" : 1,
        "$clusterTime" : {
                "clusterTime" : Timestamp(1638130938, 1),
                "signature" : {
                        "hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
                        "keyId" : NumberLong(0)
                }
        },
        "operationTime" : Timestamp(1638130938, 1)
}
MainRepSet:PRIMARY>

Se verifica en otro nodo si la colección replico
  kubectl exec -it mongod-1 -c mongod-container bash

  MainRepSet:SECONDARY> rs.secondaryOk()
  MainRepSet:SECONDARY> db.getCollectionNames()
  [ "test" ]

  • Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet,
  Cuando se habilitan el cluster las ips serian aleatorias (mongod-0.mongodb-service.default.svc.cluster.local:27017) si una cae tocaria habilitarla manualmente despues.