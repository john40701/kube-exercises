Objetivo:
1. [Horizontal Pod Autoscaler] Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas. Podéis realizar una prueba de estrés realizando un número de peticiones masivas mediante la siguiente instrucción:
        kubectl run -i --tty load-generator --rm --image=busybox --restart=Never --/bin/sh -c "while sleep 0.01; do wget -q -O- http://<svc_name>; done"

R./ Dentro de la carpeta \hw-03\exercise_3 se encuentran los siguientes ficheros:
        nginx-deployment.yaml
        hpa.yaml
        service3.yaml

        Se ejecutan los siguientes comandos:
                kubectl apply -f nginx-deployment.yaml
                kubectl apply -f service3.yaml
                kubectl apply -f hpa.yaml
        
        Se realiza una revisión del consumo minimo de nginx y se sean los valores de cpu y memoria en: "10m" y "100Mi" respectivamente, se espera que cuando se pasen del 50% de la cpu se dupliquen los pods: 
         - type: Resource
                resource:
                  name: cpu
                  target:
                    type: Utilization
                    averageUtilization: 50
        
        Se utiliza el comando para hacer las pruebas de stress:
                kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://nginx-service; done"

        Antes de lanzar el comando:
        kubectl get pods
                NAME                     READY   STATUS    RESTARTS   AGE
                nginx-5954fc5684-bhxw2   1/1     Running   0          30s
                nginx-5954fc5684-wxzc2   1/1     Running   0          30s
        
        Despues:
        kubectl get pods
                NAME                     READY   STATUS    RESTARTS   AGE
                nginx-5954fc5684-8mq6q   1/1     Running   0          55s
                nginx-5954fc5684-bhxw2   1/1     Running   0          2m32s
                nginx-5954fc5684-c4kk4   1/1     Running   0          55s
                nginx-5954fc5684-wxzc2   1/1     Running   0          2m32s
