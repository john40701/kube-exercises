Objetivo:
 Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis
utilizar la imagen del ejercicio 1.
Recordad lo que hemos visto en clase sobre “Blue Green deployment”:
a. Existe una aplicación que está desplegada en el clúster (en el ejemplo, 1.0v)

R. En la ruta /exercise_5 se encuentran los archivos: nginx-single-pod1.yaml y nginx-single-pod2.yaml
correspondientes a la versión 1 de al app, se utiliza el label app: nginx-server-v1 para identificarlos

  1. Se crean los pods:
    kubectl apply -f nginx-single-pod1.yaml
    kubectl apply -f nginx-single-pod2.yaml

    kubectl get pods -l app=nginx-server-v1
    NAME      READY   STATUS    RESTARTS   AGE
    nginx-1   1/1     Running   0          4m36s
    nginx-2   1/1     Running   0          4m15s

  2. Se crea el service serviceV1.yaml en la carpta /exercise_5 apuntando a la versión 1, en este caso el service un nodeport por la limitación de local
    kubectl apply -f serviceV1.yaml

    kubectl describe svc nginx-service
    Name:                     nginx-service
    Namespace:                default
    Labels:                   <none>
    Annotations:              <none>
    Selector:                 app=nginx-server-v1
    Type:                     NodePort
    IP Family Policy:         SingleStack
    IP Families:              IPv4
    IP:                       10.101.64.230
    IPs:                      10.101.64.230
    Port:                     <unset>  80/TCP
    TargetPort:               80/TCP
    NodePort:                 <unset>  30001/TCP
    Endpoints:                172.17.0.3:80,172.17.0.4:80
    Session Affinity:         None
    External Traffic Policy:  Cluster
    Events:                   <none>

b. Antes de ofrecer el servicio a los usuarios, la compañía necesita realizar una serie
de validaciones con la versión 2.0. Los usuarios siguen accediendo a la versión
1.0:

R. En la ruta /exercise_5 se encuentran los archivos: nginx-single-pod1-v2.yaml y nginx-single-pod2-v2.yaml
correspondientes a la versión 2 de al app, se utiliza el label app: nginx-server-v2 para identificarlos
    kubectl apply -f nginx-single-pod1-v2.yaml
    kubectl apply -f nginx-single-pod2-v2.yaml
    kubectl get pods -l app=nginx-server-v2
    NAME         READY   STATUS    RESTARTS   AGE
    nginx-1-v2   1/1     Running   0          23s
    nginx-2-v2   1/1     Running   0          15s

c. Una vez que el equipo ha validado la aplicación, se realiza un switch del tráfico a
la versión 2.0 sin impacto para los usuarios:

Se despliega el service serviceV2.yaml en la carpta /exercise_5 apuntando a la versión 2, en este caso el service un nodeport por la limitación de local
    kubectl apply -f serviceV2.yaml
    kubectl describe svc nginx-service
    Name:                     nginx-service
    Namespace:                default
    Labels:                   <none>
    Annotations:              <none>
    Selector:                 app=nginx-server-v2
    Type:                     NodePort
    IP Family Policy:         SingleStack
    IP Families:              IPv4
    IP:                       10.101.64.230
    IPs:                      10.101.64.230
    Port:                     <unset>  80/TCP
    TargetPort:               80/TCP
    NodePort:                 <unset>  30001/TCP
    Endpoints:                172.17.0.5:80,172.17.0.6:80
    Session Affinity:         None
    External Traffic Policy:  Cluster
    Events:                   <none>
    
    kubectl get pod -o wide
    NAME         READY   STATUS    RESTARTS   AGE     IP           NODE       NOMINATED NODE   READINESS GATES
    nginx-1      1/1     Running   0          21m     172.17.0.3   minikube   <none>           <none>
    nginx-1-v2   1/1     Running   0          3m58s   172.17.0.5   minikube   <none>           <none>
    nginx-2      1/1     Running   0          20m     172.17.0.4   minikube   <none>           <none>
    nginx-2-v2   1/1     Running   0          3m50s   172.17.0.6   minikube   <none>           <none>
    
    Se verifica y ahora el servicio apunta a las ips  172.17.0.5 y 172.17.0.6 correpondiente a la versión 2