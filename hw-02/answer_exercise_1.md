Objetivo:
Crea un pod de forma declarativa con las siguientes especificaciones:
    • Imagen: nginx
    • Version: 1.19.4
    • Label: app: nginx-server
    • Limits
        CPU: 100 milicores
        Memoria: 256Mi
    • Requests
        CPU: 100 milicores
        Memoria: 256Mi

R. Instrucciones para ejecutar el ejercicio:
 Se dispone los archivos:
    nginx-single-pod.yaml en la ruta /exercise_1
 Dentro de la carpeta exercise_1 se ejecutan los siguientes comandos:
    kubectl apply -f nginx-single-pod.yaml

Explicación paso a paso del ejercicio:

Se crea el siguiente archivo declarativo del pod
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: nginx-server
spec:
  containers:
  - name: nginx
    image: nginx:1.19.4
    ports:
    - containerPort: 80
    resources:
      limits:
        cpu: "100m"
        memory: "256Mi"
      requests:
        cpu: "100m"
        memory: "256Mi"s

Preguntas del Ejercicio:
Realiza un despliegue en Kubernetes, y responde las siguientes preguntas:
• ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs
generados por la aplicación)?
R./ Con el comando kubectl logs --tail=10 nginx

• ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar
el proceso que seguirías.
R./ Ejecutar el comando:
    kubectl get pod -o wide
        NAME    READY   STATUS    RESTARTS   AGE   IP           NODE       NOMINATED NODE   READINESS GATES
        nginx   1/1     Running   0          12m   172.17.0.3   minikube   <none>           <none>

    Se observa que la ip interna es: 172.17.0.3


• ¿Qué comando utilizarías para entrar dentro del pod?
    kubectl exec --stdin --tty nginx -- /bin/bash

• Necesitas visualizar el contenido que expone NGINX, ¿qué acciones
debes llevar a cabo?
    Para visualizar el contenido de forma rapida utilizaria el comando
    kubectl port-forward nginx 8090:80
    Ya para exponer el servicio, crearia un servise 

• Indica la calidad de servicio (QoS) establecida en el pod que acabas de
crear. ¿Qué lo has mirado?
 Se ejecuta el comando:
    >kubectl get pod nginx --output yaml
    y se observa que la calidad del servicio por defecto asignada es  qosClass: Guaranteed