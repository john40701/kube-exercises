Objetivo:
Crear un objeto de tipo replicaSet a partir del objeto anterior con las
siguientes especificaciones:

• Debe tener 3 replicas

R. Instrucciones para ejecutar el ejercicio:
 Se dispone los archivos:
    nginx-replicaSet.yaml /exercise_2
 Dentro de la carpeta exercise_2 se ejecutan los siguientes comandos:
    kubectl apply -f nginx-replicaSet.yaml

Explicación paso a paso del ejercicio:

    Se crea el siguiente yaml para la declaración del objeto
    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
      name: nginx
      labels:
        app: nginx-server
    spec:
      replicas: 
      selector:
        matchLabels:
          app: nginx-server
      template:
        metadata:
          labels:
            app: nginx-server
        spec:
          containers:
          - name: nginx
            image: nginx:1.19.4
            ports:
            - containerPort: 80
            resources:
              limits:
                cpu: "100m"
                memory: "256Mi"
              requests:
                cpu: "100m"
                memory: "256Mi"

Preguntas del Ejercicio:
¿Cúal sería el comando que utilizarías para escalar el número de replicas a
10?
R./ kubectl scale --replicas=10 rs/nginx

•Si necesito tener una replica en cada uno de los nodos de Kubernetes,
¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto) 
R./ Un DaemonSet