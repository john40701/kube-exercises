Objetivo:
Crea un objeto de tipo service para exponer la aplicación del ejercicio
anterior de las siguientes formas:
• Exponiendo el servicio hacia el exterior (crea service1.yaml)
  Este servicio no se puede implementar en local debido a que solo funciona en implementaciones en la nube
  Se deja declarado en el archivo service1.yml en la ruta /exercise_3 pero no se puede verificar.

• De forma interna, sin acceso desde el exterior (crea service2.yaml)
  Se crea el serchivo service2.yml en la ruta /exercise_3
    Se ejecuta:
      kubectl apply -f service2.yml

      kubectl get svc nginx-service
        NAME            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
        nginx-service   ClusterIP   10.101.229.197   <none>        80/TCP    6m34s
      
      kubectl describe svc nginx-service
        Name:              nginx-service
        Namespace:         default
        Labels:            <none>
        Annotations:       <none>
        Selector:          app=nginx-server
        Type:              ClusterIP
        IP Family Policy:  SingleStack
        IP Families:       IPv4
        IP:                10.101.229.197
        IPs:               10.101.229.197
        Port:              <unset>  80/TCP
        TargetPort:        80/TCP
        Endpoints:         172.17.0.3:80,172.17.0.4:80,172.17.0.6:80
        Session Affinity:  None
        Events:            <none>

• Abriendo un puerto especifico de la VM (crea service3.yaml)
  Se crea el serchivo service3.yml en la ruta /exercise_3
    Se ejecuta:
      kubectl apply -f service3.yml

      kubectl get svc nginx-service
        NAME            TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
        nginx-service   NodePort   10.105.226.139   <none>        80:30007/TCP   32m
      
      kubectl describe svc nginx-service
        Name:                     nginx-service
        Namespace:                default
        Labels:                   <none>
        Annotations:              <none>
        Selector:                 app=nginx-server
        Type:                     NodePort
        IP Family Policy:         SingleStack
        IP Families:              IPv4
        IP:                       10.105.226.139
        IPs:                      10.105.226.139
        Port:                     <unset>  80/TCP
        TargetPort:               80/TCP
        NodePort:                 <unset>  30007/TCP
        Endpoints:                172.17.0.3:80,172.17.0.4:80,172.17.0.6:80
        Session Affinity:         None
        External Traffic Policy:  Cluster
        Events:                   <none>

        

