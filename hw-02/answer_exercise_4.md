Objetivo:
Crear un objeto de tipo deployment con las especificaciones del ejercicio 1.
• Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”
  R. Instrucciones para ejecutar el ejercicio:

   1. Se crea el deployment inicial utilizando el archivo nginx-deployment.yaml ubilicado en la ruta /excercise_4
      kubectl apply -f nginx-deployment.yaml 
      kubectl get pods
        NAME                     READY   STATUS    RESTARTS   AGE
        nginx-5cfbbf5d48-j962h   1/1     Running   0          4m4s
        nginx-5cfbbf5d48-krllr   1/1     Running   0          4m4s
        nginx-5cfbbf5d48-nvtt8   1/1     Running   0          4m4s

   2. Se realiza un downgrade de la versión de nginx y de despliega utilizando la strategia recreate
   utilizando el archivo archivo nginx-deployment-recreate.yaml ubilicado en la ruta /excercise_4
      kubectl apply -f nginx-deployment-recreate.yaml
      kubectl get deployments
        NAME    READY   UP-TO-DATE   AVAILABLE   AGE
        nginx   0/3     3            0           4m21s
    
    Se observa que los pods fueron eliminados
  

• Despliega una nueva versión haciendo “rollout deployment”

 1. Se crea el deployment inicial utilizando el archivo nginx-deployment.yaml ubilicado en la ruta /excercise_4
      kubectl apply -f nginx-deployment.yaml 
      kubectl get pods
        NAME                     READY   STATUS    RESTARTS   AGE
        nginx-5cfbbf5d48-j9jpc   1/1     Running   0          9s
        nginx-5cfbbf5d48-tw778   1/1     Running   0          21s
        nginx-5cfbbf5d48-wwkzj   1/1     Running   0          15s

   2. Se realiza un downgrade de la versión de nginx y de despliega utilizando la strategia recreate
   utilizando el archivo archivo nginx-deployment-rollout.yaml ubilicado en la ruta /excercise_4
    kubectl get pods
        NAME                     READY   STATUS              RESTARTS   AGE
        nginx-5cfbbf5d48-j9jpc   1/1     Running             0          3m29s
        nginx-5cfbbf5d48-tw778   1/1     Running             0          3m41s
        nginx-5cfbbf5d48-wwkzj   1/1     Terminating         0          3m35s
        nginx-66667d58d5-7lrmp   0/1     ContainerCreating   0          3s
        nginx-66667d58d5-9bjh8   1/1     Running             0          9s

      Se observa que va iniciando pods nuevos y a medida que van iniciando se eliminan los antiguos

• Realiza un rollback a la versión generada previamente
Se revisan las revisiones
    kubectl rollout history deploy nginx
    deployment.apps/nginx
    REVISION  CHANGE-CAUSE
    4         <none>
    5         <none>
    6         <none>

  Se realiza rollback a la revision 4

  >kubectl rollout history deploy nginx --revision=4
deployment.apps/nginx with revision #4
Pod Template:
  Labels:       app=nginx-server
        pod-template-hash=66667d58d5
  Containers:
   nginx:
    Image:      nginx:1.14.2
    Port:       80/TCP
    Host Port:  0/TCP
    Limits:
      cpu:      100m
      memory:   256Mi
    Requests:
      cpu:      100m
      memory:   256Mi
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>

>kubectl rollout undo deploy nginx --to-revision=4
deployment.apps/nginx rolled back

kubectl get pods
NAME                     READY   STATUS              RESTARTS   AGE
nginx-5cfbbf5d48-47psm   1/1     Running             0          8m18s
nginx-5cfbbf5d48-hhwfs   1/1     Terminating         0          8m24s
nginx-66667d58d5-2bwlm   1/1     Running             0          14s
nginx-66667d58d5-rthvm   1/1     Running             0          9s
nginx-66667d58d5-s5ccf   0/1     ContainerCreating   0          2s

kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-66667d58d5-2bwlm   1/1     Running   0          4m18s
nginx-66667d58d5-rthvm   1/1     Running   0          4m13s
nginx-66667d58d5-s5ccf   1/1     Running   0          4m6s

Se verifica que los pods resultanten pertenecen al hash 66667d58d5 correspondiente a la versión 4